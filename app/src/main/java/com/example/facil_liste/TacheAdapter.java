package com.example.facil_liste;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.facil_liste.model.Tache;

import java.util.List;


public class TacheAdapter extends BaseAdapter {
    Activity mActivity;
    List<Tache> mTachesList;

    //Constructeur
    public TacheAdapter(Activity activity, List<Tache> taches) {
        this.mActivity = activity;
        this.mTachesList = taches;
    }


    //Renvoie le nombre d'items représentés par l'adapter
    @Override
    public int getCount() {
        return this.mTachesList.size();
    }

    //Renvoie l'item associé à cette position dans l'adapter
    @Override
    public Object getItem(int position) {
        return this.mTachesList.get(position);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    //La vue pour chaque ligne, on set ses composants
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) this.mActivity.getLayoutInflater();
        View uneTacheLigne = inflater.inflate(R.layout.row, parent, false);

        TextView tvTitre = uneTacheLigne.findViewById(R.id.tv_titre);
        TextView tvDescription = uneTacheLigne.findViewById(R.id.tv_description);

        Tache t = (Tache) this.getItem(position);

        tvTitre.setText(t.getmTitre());
        tvDescription.setText(t.getmDescription());

        return uneTacheLigne;
    }
}
