package com.example.facil_liste.data;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.example.facil_liste.model.Tache;
import com.example.facil_liste.util.Util;

import java.util.ArrayList;
import java.util.List;

public class DatabaseHandler extends SQLiteOpenHelper {
    private SQLiteDatabase mDb;

    public DatabaseHandler(Context contexte) {
        super(contexte, Util.DATABASE_NAME, null, Util.DATABASe_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_TACHE_TABLE = "CREATE TABLE " + Util.TABLE_NAME + "("
                + Util.KEY_ID + " INTEGER PRIMARY KEY, "
                + Util.KEY_TITRE + " TEXT, "
                + Util.KEY_DESCRIPTION + " TEXT"
                + ")";

        db.execSQL(CREATE_TACHE_TABLE);

    }

    //Appelée lorsque le numéro de version ne correspond pas à celui spécifié dans Utils
    @Override
    public void onUpgrade(SQLiteDatabase db, int versPrecedante, int versSuivante) {
        String DROP_TABLE = "DROP TABLE IF EXISTS ";
        db.execSQL(DROP_TABLE + Util.TABLE_NAME);

        onCreate(db);
    }


    //Ouvrir la connexion
    public void open() {
        this.mDb = this.getWritableDatabase();
    }

    //Fermer la connexion
    public void close() {
        this.mDb.close();
    }

    public boolean verifierTacheExiste(int id) {
        String select = "SELECT * FROM " + Util.TABLE_NAME + " WHERE " + Util.KEY_ID + "=" + id;
        Cursor curseur = mDb.rawQuery(select, null);
        Log.i("BD", "Id de la tâche : " + String.valueOf(id));


        //Vérifier si la BD est vide
        boolean present = (curseur.getCount() > 0);
        Log.i("BD", "Présent? " + String.valueOf(present));
        curseur.close();
        return present;
    }

    public void ajouterTache(Tache tache) {
        ContentValues valeurs = new ContentValues();
        //Si
        if (tache.getmId() != null)
            valeurs.put(Util.KEY_ID, tache.getmId());

        valeurs.put(Util.KEY_TITRE, tache.getmTitre());
        valeurs.put(Util.KEY_DESCRIPTION, tache.getmDescription());
        Log.i("BD", "KEY_ID : " + String.valueOf(Util.KEY_ID));
        //TODO : ajouter image?

        this.mDb.insert(Util.TABLE_NAME, null, valeurs);

        //TODO : mDb.close()???
    }

    public Tache getTache(int id) {
        Cursor curseur = mDb.query(Util.TABLE_NAME,
                new String[]{Util.KEY_ID, Util.KEY_TITRE, Util.KEY_DESCRIPTION},
                Util.KEY_ID + "=?", new String[]{String.valueOf(id)},
                null, null, null);
        curseur.moveToFirst();

        Tache tache = null;
        //Vérifier si la BD est vide
        if (!curseur.isAfterLast()) {
            tache = lireLigne(curseur);
        }
        curseur.close();
        return tache;

    }

    public List<Tache> getAllTaches() {
        List<Tache> tachesList = new ArrayList<>();
        String selectAll = "SELECT * FROM " + Util.TABLE_NAME;
        Cursor cursor = mDb.rawQuery(selectAll, null);


        if (cursor.moveToFirst()) {
            do {
                Tache tache = lireLigne(cursor);
                tachesList.add(tache);
            } while (cursor.moveToNext());
        }
        cursor.close();
        return tachesList;
    }

    //
//    public int majTache(Tache tache) {
//
//    }
//
    public void supprimerTache(Tache tache) {
        mDb.delete(Util.TABLE_NAME,
                Util.KEY_ID + "=?",
                new String[]{String.valueOf(tache.getmId())});
        //TODO:  mbd.close() nécessaire?
    }

    public void supprimerToutesTaches() {
        mDb.execSQL("delete from " + Util.TABLE_NAME);
    }

    private static Tache lireLigne(Cursor curseur) {
        Tache t = new Tache();
        t.setmId(curseur.getInt(Util.IDX_ID));
        t.setmTitre(curseur.getString(Util.IDX_TITRE));
        t.setmDescription(curseur.getString(Util.IDX_DESCRIPTION));
        //TODO : image dans la BD?
        return t;
    }

}
