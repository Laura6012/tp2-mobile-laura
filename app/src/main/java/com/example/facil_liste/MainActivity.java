package com.example.facil_liste;

import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.recyclerview.widget.RecyclerView;

import com.example.facil_liste.data.DatabaseHandler;
import com.example.facil_liste.model.Tache;
import com.example.facil_liste.ui.DialogTacheFragment;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, DialogTacheFragment.EditTacheDialogListener {

    private final int REQUEST_CODE = 1234;

    Toolbar tbar;
    AppBarConfiguration appBarConfiguration;
    ImageButton btnToutes;
    ImageButton btnChoisies;
    Button btnAjouter;
    FragmentManager manager;
    FragmentTachesDispos fragmentDispos;
    FragmentTachesChoisies fragmentChoisies;

    TacheRecyclerAdapter adapter;
    RecyclerView rvTaches;
    //    List<Tache> taches;
    DatabaseHandler databaseHandler;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Log.i("TAG", "onCreate MainActivity");

        //Les composants
        btnToutes = findViewById(R.id.btn_toutes);
        btnChoisies = findViewById(R.id.btn_choisies);
        btnAjouter = findViewById(R.id.btn_ajouter);

        //Les écouteurs
        btnToutes.setOnClickListener(this);
        btnChoisies.setOnClickListener(this);
        btnAjouter.setOnClickListener(this);

        //Toolbar
        tbar = findViewById(R.id.tbar);
        setSupportActionBar(tbar);
        getSupportActionBar().setTitle(getString(R.string.app_name));
        getSupportActionBar().setLogo(R.drawable.logo_checklist);


        //Fragments
        fragmentDispos = new FragmentTachesDispos();
        fragmentChoisies = new FragmentTachesChoisies();

        manager = getSupportFragmentManager();


        List<Fragment> fragments = new ArrayList<>();
        fragments.add(fragmentChoisies);
        fragments.add(fragmentDispos);


//        loadFragments();
        FragmentTransaction transaction = manager.beginTransaction();
        if (fragmentDispos != null) {
            transaction.add(R.id.fl_conteneur, fragmentDispos, "fragmentDispos")
                    .addToBackStack(null)
                    .commit();
        }

    }


//    private void loadFragments() {
//        FragmentTransaction transaction = manager.beginTransaction();
//
//        if (fragmentChoisies != null) {
//            transaction.add(R.id.fl_conteneur, fragmentChoisies, "fragmentChoisies")
//                    .addToBackStack(null)
//                    .commit();
//        }
//        if (fragmentDispos != null) {
//            transaction.add(R.id.fl_conteneur, fragmentDispos, "fragmentDispos")
//                    .addToBackStack(null)
//                    .commit();
//        }


//        getSupportFragmentManager()
//                .beginTransaction()
//                .show(fragmentDispos);

//}

    private void hideFragmentDispos() {
        FragmentTachesDispos fragmentDispos = (FragmentTachesDispos) manager.findFragmentByTag("fragmentDispos");
        manager.beginTransaction()
                .hide(fragmentDispos)
                .commit();

        Log.i("TAG", "hide fragment dispo");
    }

    private void showFragmentDispo() {
        FragmentTachesDispos fragmentDispos = (FragmentTachesDispos) manager.findFragmentByTag("fragmentDispos");
        manager.beginTransaction()
                .show(fragmentDispos)
                .commit();
        Log.i("TAG", "show fragment dispo");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //Inflater le main_menu dans le menu
        getMenuInflater().inflate(R.menu.main_menu, menu);

        return true;
    }

    //Quand on clique sur une des options du menu
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        switch (item.getItemId()) {
            case R.id.i_admin:
                return true;

            case R.id.i_tout_supp:
                fragmentDispos.taches.clear();
                fragmentDispos.adapter.notifyDataSetChanged();

                Bundle bundle = new Bundle();

                fragmentDispos.databaseHandler.supprimerToutesTaches();

                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }

    @Override
    public void onClick(View view) {


        switch (view.getId()) {
            case R.id.btn_toutes:

                showFragmentDispo();
                break;
            case R.id.btn_choisies:

                hideFragmentDispos();
                break;
            case R.id.btn_ajouter:
//                Intent intention = new Intent(MainActivity.this, NouvelleTacheActivity.class);
//                startActivityForResult(intention, REQUEST_CODE);
                Log.i("TAG", "CLIC AJOUTER");

                DialogTacheFragment dialogFragment = new DialogTacheFragment();
                dialogFragment.show(getSupportFragmentManager(), "AddTache");
                break;
        }

    }

    // Méthode de l'interface EditPersonDialogListener pour récupérer les informations de la popup
    // quand on modifie une personne
    @Override
    public void onFinishEditDialog(Tache tache, int position) {
        //Si c'est une modification, on retire l'ancienne version de la tâche avant de la rajouter
        if (position > -1) {
            fragmentDispos.taches.remove(position);
        }
        fragmentDispos.taches.add(tache);
        fragmentDispos.databaseHandler.ajouterTache(tache);
        fragmentDispos.adapter.notifyDataSetChanged();

    }

//
//    protected void onActivityResult(int req, int res, Intent data) {
//        super.onActivityResult(req, res, data);
//
//        if (req == REQUEST_CODE) {
//            if (res == RESULT_OK) {
//                int position = data.getIntExtra("position", -1);
//                String titre = data.getStringExtra("titre");
//                String description = data.getStringExtra("description");
//
//
//                if (position > -1) {
//                    //Retirer la tache qui se trouve à cette position
//                    taches.remove(position);
//                }
//
//                // crée un nouvel objet Person
//                Tache t = new Tache(null, titre, description);
//                //TODO : ajouter directement dans la BD??
//                // ajoute à la liste de personnes
//                taches.add(t);
//
//                adapter.notifyDataSetChanged();
//
//            } else {
//                // TODO
//            }
//        }
//
//
//    }

}

