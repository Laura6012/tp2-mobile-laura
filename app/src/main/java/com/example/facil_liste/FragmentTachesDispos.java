package com.example.facil_liste;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.facil_liste.data.DatabaseHandler;
import com.example.facil_liste.model.Tache;

import java.util.ArrayList;
import java.util.List;

public class FragmentTachesDispos extends Fragment {

    List<Tache> taches;
    RecyclerView rvTaches;
    TacheRecyclerAdapter adapter;
    DatabaseHandler databaseHandler;


    //Ouverture de la BD lorsque l'app démarre
    @Override
    public void onStart() {
        this.databaseHandler.open();
        super.onStart();
        Log.i("TAG", "Dans le onStart du fragmentDispos");

    }

    //Fermeture de la BD lorsque l'app se s'arrête
    @Override
    public void onStop() {
        this.databaseHandler.close();
        super.onStop();
        Log.i("TAG", "Dans le onStop du fragmentDispos");
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        Log.i("TAG", "Dans le onAttach du fragment");
    }

    // Note : on passe icie UNE SEULE FOIS, au démarrage
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.i("TAG", "Dans le onCreate du fragment");

        //Base de données
        databaseHandler = new DatabaseHandler(getContext());
        databaseHandler.open();//TODO nécessaire??
        databaseHandler.supprimerToutesTaches();

        List<Tache> taches = new ArrayList<>();
        taches.add(new Tache(1, "Nage", "Aller à la piscine 2x"));
        taches.add(new Tache(2, "Faire l'épicerie", "Pas de viande"));
        taches.add(new Tache(3, "Appeler le docteur", "Avant midi"));

        // Vérifier si les 3 tâches par défaut dont présentes dans la BD. Sinon, les ajouter
        for (Tache t : taches) {
            if (!databaseHandler.verifierTacheExiste(t.getmId()))
                databaseHandler.ajouterTache(t);
        }

    }

    // Note : on repasse par ici après que la vue ait été détruite (ex. : changer de fragment)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Log.i("TAG", "FragmentTachesDispo : onCreateView");

        View view = inflater.inflate(R.layout.fragment_taches_dispos, container, false);
        return view;
    }

    // Note : on repasse par ici après que la vue ait été détruite (ex. : changer de fragment)
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Log.i("TAG", "FragmentTachesDispo : onActivityCreated");


        //Construction du RecylcerView
        buildRecyclerView();

    }

    //On va chercher la liste dans la BD
    private void buildRecyclerView() {

        Log.i("TAG", "Le RecyclerView se construit");
        //Trouver le composant
        rvTaches = getView().findViewById(R.id.rv_dispos);
        rvTaches.setHasFixedSize(true);


        //Aller chercher la liste dans la BD
        taches = databaseHandler.getAllTaches();
        Log.i("TAG", "nb de taches dans la BD : " + String.valueOf(taches.size()));
        //Setter l'adapter
        adapter = new TacheRecyclerAdapter(taches);
        rvTaches.setAdapter(adapter);
        rvTaches.setLayoutManager(new LinearLayoutManager(getContext()));

        registerForContextMenu(rvTaches);//TODO nécessaire??

        //Implémentation des écouteurs de TacheRecyclerAdapter
        adapter.setOnClickListener(new TacheRecyclerAdapter.onItemClickListener() {
            @Override
            public void onItemClick(int position) {

            }

            @Override
            public void onClickDelete(int position) {

            }
        });

    }

}
