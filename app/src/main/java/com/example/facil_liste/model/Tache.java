package com.example.facil_liste.model;

import androidx.room.Entity;


public class Tache {
    private Integer mId;
    private String mTitre;
    private String mDescription;

    public Tache() {
    }

    public Tache(Integer id, String titre, String description) {
        this.mId = id;
        this.mTitre = titre;
        this.mDescription = description;
    }

    public Integer getmId() {
        return mId;
    }

    public void setmId(Integer mId) {
        this.mId = mId;
    }

    public String getmTitre() {
        return mTitre;
    }

    public void setmTitre(String mTitre) {
        this.mTitre = mTitre;
    }

    public String getmDescription() {
        return mDescription;
    }

    public void setmDescription(String mDescription) {
        this.mDescription = mDescription;
    }


}
