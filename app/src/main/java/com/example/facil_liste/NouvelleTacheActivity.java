//package com.example.facil_liste;
////
////import android.content.Intent;
////import android.os.Bundle;
////import android.view.View;
////import android.widget.Button;
////import android.widget.EditText;
////
////import androidx.appcompat.app.AppCompatActivity;
////
////public class NouvelleTacheActivity extends AppCompatActivity implements View.OnClickListener {
////
////    Button btnOK, btnAnnuler;
////    EditText etTitre, etDescription;
////    int mPosition = -1;
////
////    @Override
////    protected void onCreate(Bundle savedInstanceState) {
////        super.onCreate(savedInstanceState);
////        setContentView(R.layout.creer_modifier_tache);
////        btnOK = findViewById(R.id.btn_ok);
////        btnAnnuler = findViewById(R.id.btn_annuler);
////        etTitre = findViewById(R.id.et_titre);
////        etDescription = findViewById(R.id.et_Description);
////
////        //Ajout d'écouteurs
////        btnOK.setOnClickListener(this);
////        btnAnnuler.setOnClickListener(this);
////
////    }
////
////    @Override
////    public void onClick(View view) {
////        switch (view.getId()) {
////            case R.id.btn_ok:
////                //On renvoie dans une intention les données de la nouvelle tâche
////                String nouvTitre = etTitre.getText().toString();
////                String nouveDescription = etDescription.getText().toString();
////
////                Intent intention = new Intent(NouvelleTacheActivity.this, MainActivity.class);
////                intention.putExtra("position", mPosition);
////                intention.putExtra("titre", nouvTitre);
////                intention.putExtra("description", nouveDescription);
////
////                setResult(RESULT_OK, intention);
////                finish();
////                break;
////            case R.id.btn_annuler:
////                break;
////        }
////    }
////}