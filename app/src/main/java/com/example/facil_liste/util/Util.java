package com.example.facil_liste.util;

public class Util {
    //Éléments liés à la BD
    public static final int DATABASe_VERSION = 1;
    public static final String DATABASE_NAME = "tache_db";
    public static final String TABLE_NAME = "tache_db";

    //Colonnes de la BD
    public static final String KEY_ID = "id";
    public static final String KEY_TITRE = "titre";
    public static final String KEY_DESCRIPTION = "description";
    //TODO : Ajouter image
    //Constantes pour les indices des champs dans la BD
    public static final int IDX_ID = 0;
    public static final int IDX_TITRE = 1;
    public static final int IDX_DESCRIPTION = 2;
}
