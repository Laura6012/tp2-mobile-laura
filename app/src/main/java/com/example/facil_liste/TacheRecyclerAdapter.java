package com.example.facil_liste;


import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.facil_liste.model.Tache;

import java.util.List;

public class TacheRecyclerAdapter extends RecyclerView.Adapter<TacheRecyclerAdapter.ViewHolder> {
    private List<Tache> taches;
    private onItemClickListener mListener;

    public void setOnClickListener(onItemClickListener listener) {
        this.mListener = listener;
    }

    public interface onItemClickListener {
        void onItemClick(int position);

        void onClickDelete(int position);
    }

    //---------------------------------------Début classe interne------------
    public static class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvTitre, tvDescription;

        public ViewHolder(@NonNull View itemView, final onItemClickListener listener) {
            super(itemView);
            tvTitre = itemView.findViewById(R.id.tv_titre);
            tvDescription = itemView.findViewById(R.id.tv_description);

            //  Écouteur sur un clic
            itemView.setOnClickListener((new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null) {
                        int position = getAdapterPosition();

                        if (position != RecyclerView.NO_POSITION) {
                            listener.onItemClick(position);
                        }
                    }
                }
            }));

            //  Écouteur pour clic long
            itemView.setOnCreateContextMenuListener(new View.OnCreateContextMenuListener() {
                @Override
                public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
//                    MenuItem delete = menu.add(0, v.getId(), 0, R.string.Supprimer);
//
//                    delete.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
//                        @Override
//                        public boolean onMenuItemClick(MenuItem item) {
//                            int position = getAdapterPosition();
//                            listener.onClickDelete(position);
//                            return false;
//                        }
//                    });
                }
            });


        }
    }
//-----------------------------------Fin classe interne

    //Constructeur
    public TacheRecyclerAdapter(List<Tache> taches) {
        this.taches = taches;
    }

    // Méthode qui permet de créer une ligne
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row, parent, false);
        return new ViewHolder(view, mListener);
    }

    // Méthode qui remplit une ligne créée
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Tache tache = this.taches.get(position);

        holder.tvTitre.setText(tache.getmTitre());
        holder.tvDescription.setText(tache.getmDescription());

    }

    // Méthode qui indique le nombre de ligne à créer
    @Override
    public int getItemCount() {
        return this.taches.size();
    }

    //Pour supprimer toutes les lignes dans l'adapter




}
