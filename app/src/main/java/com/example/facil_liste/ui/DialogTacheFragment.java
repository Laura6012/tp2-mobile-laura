package com.example.facil_liste.ui;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import androidx.fragment.app.DialogFragment;

import com.example.facil_liste.R;
import com.example.facil_liste.model.Tache;


public class DialogTacheFragment extends DialogFragment {
    private Tache mTache;
    private int mPosition = -1;

    // Interface pour envoyer la tâche modifiée dans la MainActivity
    public interface EditTacheDialogListener {
        void onFinishEditDialog(Tache tache, int position);
    }

    //Constructeur pour une nouvelle tâche
    public DialogTacheFragment() {

    }

    //Constructeur pour une tâche à modifier
    public DialogTacheFragment(Tache tache, int position) {
        this.mTache = tache;
        this.mPosition = position;

    }


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        String textButtonAdd = "Ajouter";
        if (mPosition > -1) {
            textButtonAdd = "Modifier";
        }

        // Use the Builder class for convenient dialog construction
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = requireActivity().getLayoutInflater();
        final View v = inflater.inflate(R.layout.creer_modifier_tache, null);
        final Context context = getContext();


        //  Si la tache est nulle, alors c'est de la modification
        if (mTache != null) {
            EditText etTitre = v.findViewById(R.id.et_titre);
            EditText etDescription = v.findViewById(R.id.et_description);
            //TODO : le numéro de photo
            etTitre.setText(mTache.getmTitre());
            etDescription.setText(mTache.getmDescription());
        }

        builder.setView(v).setPositiveButton(textButtonAdd, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                EditText etTitre = v.findViewById(R.id.et_titre);
                EditText etDescription = v.findViewById(R.id.et_description);
                //TODO : le numéro de photo
                String titre = etTitre.getText().toString().trim();
                String description = etDescription.toString().trim();

                if (titre.isEmpty() || description.isEmpty()) {
                    Toast.makeText(context, R.string.error_edit_tache, Toast.LENGTH_SHORT).show();
                    return;
                }
                //Sans id, il sera rajouté lors de l'insertion dans la BD
                Tache tache = new Tache();
                tache.setmTitre(titre);
                tache.setmDescription(description);
                //TODO : le num de photo

                EditTacheDialogListener listener = (EditTacheDialogListener) getActivity();
                listener.onFinishEditDialog(tache, mPosition);
            }
        }).setNegativeButton(R.string.annuler, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                //On annule la création/modification d'une tâche
            }
        });

        // Create the AlertDialog object and return it
        if (mPosition > -1) {
            builder.setTitle(R.string.modifier_tache);
        } else {
            builder.setTitle(R.string.ajouter_tache);
        }
        return builder.create();
    }


}


